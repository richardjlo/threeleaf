Site builder for doctor websites

GETTING STARTED:
1. Make sure node is installed. Install node via homebrew
	- Download homebrew: http://brew.sh/
	- go to terminal and do "brew install node"
2. Checkout this repository, and navigate to the directory
3. Type "node app.js" in terminal
4. Now your app is running at http://localhost:3000
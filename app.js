var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.favicon(path.join(__dirname, 'public/img/favicon.ico'))); //Note: Why is this not working????
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.cookieParser('super secret 3952818g8h38gh'));
app.use(express.session());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));



// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Renders homepage
app.get('/', routes.index);

// Renders checkout page
app.get('/checkout', routes.checkout);

// app.get('/success', routes.success); //temporary for testing

// Posts checkout information to server
app.post('/checkout', routes.payment);






http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

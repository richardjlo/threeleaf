// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://manage.stripe.com/account
//var api_key = 'sk_08baECW3ChbJIhIh1yxC7yfDpWilr'; //secret test
var api_key = 'sk_live_JJh8N2DEddgOHEdQZpA73b7P';
var stripe = require('stripe')(api_key);
var sendgrid  = require('sendgrid')('richardjlo', 'WKxI3Um3iH');


exports.index = function(req, res) {
	res.render('index', { title: "Three Leaf Tea" });
};

exports.checkout = function(req, res) {
	res.render('checkout', { title: "Checkout" });
};

//temporary for testing
exports.success = function(req, res) {
	res.render('success', { title: "success" });
};

exports.payment = function(req, res) {
	// console.log(req.body);

	// Get the credit card details submitted by the form
	var token = req.body.stripeToken;
	var set = parseInt(req.body.optionsRadios);
	var name = req.body.name;
	var email = req.body.email;      
	var address = req.body.address;
	var city = req.body.city;
	var state = req.body.state;
	var zip = req.body.zip;

	var fullAddress = address + " " + city + " " + state + " " + zip;
	var description = name + ' :: ' + fullAddress;

	var invoicePrice;

	var account_balance;
	if (set == 1) {
		account_balance = 4000;
		invoicePrice = "$69.00";
	} else {
		account_balance = 0;
		invoicePrice = "$29.00";
	};

	//Create a Customer   
	stripe.customers.create(
		{
			card : token,	 
			email : email,
			description : description,
			account_balance: account_balance
		}, function(err, customer) {
			if (err) {
				console.log(err.message);
				return;
			}
				// console.log("customer id", customer.id);            	     
				stripe.customers.update(customer.id, {
					plan: 'standard'
				}, function(err) {
					if (err) {
						console.log(err.message);
						return;		
					};
				}
				);
			}
	);


	// Send email receipt
	var currentDate = new Date();
	var dd = currentDate.getDate();
	var mm = currentDate.getMonth()+1; //January is 0!

	var yyyy = currentDate.getFullYear();
	if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} currentDate = mm+'/'+dd+'/'+yyyy;



	var payload   = {
	  to      : email, 
	  from    : 'threeleaftea.com@gmail.com',
	  subject : 'Three Leaf Tea Order Confirmation',
	  text    : 'Three Leaf Tea Invoice \n\nBilling Date: ' + currentDate +'\n\nOrder: Monthly Tea Subscription ' + invoicePrice + '\n\nThank you for subscribing to Three Leaf Tea! We have received your payment and are preparing your order for delivery.\n\nYou can update or cancel your order anytime by emailing us at threeleaftea.com@gmail.com.\n\nThanks, \n\nThree Leaf Tea'
	}

	sendgrid.send(payload, function(err, json) {
	  if (err) { console.error(err); }
	  console.log(json);
	});	


	
	res.render('success', { title: 'Order Success'})

};

























